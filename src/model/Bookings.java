/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author User
 */
public class Bookings 
{
    
    
    public static final String WAITING = "Waiting";
    public static final String IN_PROGRESS = "In Progress";
    public static final String FINISH = "Finish";
    
    private ArrayList<Specialist> Specialist;
    private Patient Patient;
    private String Desired_Date;
    private String Status;
    private String Code;
    private int Slot;    

    public Bookings(ArrayList<Specialist> Specialist, Patient Patient, String Desired_Date, String Status, String Code, int Slot) {
        this.Specialist = Specialist;
        this.Patient = Patient;
        this.Desired_Date = Desired_Date;
        this.Status = Status;
        this.Code = Code;
        this.Slot = Slot;
    }

    public String getDesired_Date() {
        return Desired_Date;
    }

    public void setDesired_Date(String Desired_Date) {
        this.Desired_Date = Desired_Date;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }
    
    public ArrayList<Specialist> getSpecialist() {
        return Specialist;
    }

    public void setSpecialist(ArrayList<Specialist> Specialist) {
        this.Specialist = Specialist;
    }

    public Patient getPatient() {
        return Patient;
    }

    public void setPatient(Patient Patient) {
        this.Patient = Patient;
    }
    
    public int getSlot() {
        return Slot;
    }

    public void setSlot(int Slot) {
        this.Slot = Slot;
    }
    
}
