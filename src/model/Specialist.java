/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;


/**
 *
 * @author User
 */
public class Specialist {
    
    private int Specialist_Id;
    private String Specialist_Name;
    private ArrayList<Bookings> Bookings;

    public Specialist(int Specialist_Id, String Specialist_Name) {
        this.Specialist_Id = Specialist_Id;
        this.Specialist_Name = Specialist_Name;
        Bookings = new ArrayList<>();
    }

    public ArrayList<Bookings> getBookings() {
        return Bookings;
    }

    public void setBookings(ArrayList<Bookings> Bookings) {
        this.Bookings = Bookings;
    }
    
    

    public int getSpecialist_Id() {
        return Specialist_Id;
    }

    public void setSpecialist_Id(int Specialist_Id) {
        this.Specialist_Id = Specialist_Id;
    }

    public String getSpecialist_Name() {
        return Specialist_Name;
    }

    public void setSpecialist_Name(String Specialist_Name) {
        this.Specialist_Name = Specialist_Name;
    }
     
}
