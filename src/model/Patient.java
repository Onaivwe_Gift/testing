/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import onlinebookingsystem.OnlineBookingSystem;
/**
 *
 * @author User
 */
public class Patient {
    
    private int Hospital_Id;
    private String Name;    
    private ArrayList<Bookings> Bookings;

    public Patient(int Hospital_Id, String Name) {
        this.Hospital_Id = Hospital_Id;
        this.Name = Name;
        this.Bookings = Bookings;
    }

    
    public int getHospital_Id() {
        return Hospital_Id;
    }

    public void setHospital_Id(int Hospital_Id) {
        this.Hospital_Id = Hospital_Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    
    public ArrayList<Bookings> getBookings() {
        return Bookings;
    }

    public void setBookings(ArrayList<Bookings> Bookings) {
        this.Bookings = Bookings;
    }
    
    
    public static Patient get(int id)
    {
        ArrayList<Patient> p = OnlineBookingSystem.patientInfo;
        for(int i = 0; i < p.size(); i++){
            if(p.get(i).getHospital_Id() == id){
                return p.get(i);
            }
        }
        return null;
    }

    
}
