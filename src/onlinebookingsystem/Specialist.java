/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author User
 */
public class Specialist extends JFrame
{
    private javax.swing.JButton jbtViewSpecialistAppointment;
    private javax.swing.JComboBox jcbSelectSpecialist;
    private javax.swing.JLabel jblSelectSpecialist;
    
    Specialist()
    {
        jblSelectSpecialist = new javax.swing.JLabel();
        jcbSelectSpecialist = new javax.swing.JComboBox();
        jbtViewSpecialistAppointment = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Specialist");

        jblSelectSpecialist.setText("Select Specialist:");

        jbtViewSpecialistAppointment.setText("View all specialist appointment");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jbtViewSpecialistAppointment)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jblSelectSpecialist, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jcbSelectSpecialist, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jblSelectSpecialist, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcbSelectSpecialist, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jbtViewSpecialistAppointment)
                .addContainerGap(23, Short.MAX_VALUE))
        );
        
        jbtViewSpecialistAppointment.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                SpecialistAppiontment specialistAppiontmentFrame = new SpecialistAppiontment();
                specialistAppiontmentFrame.setBounds(500,100,400,200);
                specialistAppiontmentFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                
            }
        });

        pack();
        setVisible(true);
    }
    
}
