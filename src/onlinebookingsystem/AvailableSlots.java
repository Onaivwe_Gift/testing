/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import java.util.ArrayList;
import javax.swing.JFrame;
import model.Specialist;

/**
 *
 * @author User
 */
public class AvailableSlots extends JFrame
{
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox jcbSpecialistType;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JTextField jTextField1;
    
    ArrayList<Specialist> specialists = OnlineBookingSystem.specialistType;
    
    AvailableSlots()    // the frame constructor
    {
//        for(int i=0; i<specialists.size(); i++){
//            jcbSpecialistType.addItem(specialists.get(i).getSpecialist_Name());
//        }
        
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jRadioButton3 = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jRadioButton4 = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jcbSpecialistType = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Schedule Appoint");
        getContentPane().setLayout(new java.awt.GridLayout(2, 5));

        jPanel1.setLayout(new java.awt.GridLayout(3, 2));

        jLabel3.setText("Slot Date");
        jPanel1.add(jLabel3);

        jLabel4.setText("Slot Code");
        jPanel1.add(jLabel4);

        jRadioButton3.setText("21/3/2015");
        jPanel1.add(jRadioButton3);

        jLabel1.setText("CM008AM");
        jPanel1.add(jLabel1);

        jRadioButton4.setText("12/3/2015");
        jPanel1.add(jRadioButton4);

        jLabel2.setText("CM010AM");
        jPanel1.add(jLabel2);

        getContentPane().add(jPanel1);

        jPanel2.setLayout(new java.awt.GridLayout(2, 2));

        jLabel5.setText("Desired date:");
        jPanel2.add(jLabel5);
        jPanel2.add(jTextField1);

        jLabel6.setText("Select Specialist:");
        jPanel2.add(jLabel6);

        jcbSpecialistType.setMaximumSize(new java.awt.Dimension(327, 327));
        jcbSpecialistType.setMinimumSize(new java.awt.Dimension(0, 0));
        jPanel2.add(jcbSpecialistType);

        getContentPane().add(jPanel2);

        pack();
        
        setVisible(true);
         
    }
}
