package onlinebookingsystem;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import model.Bookings;
import model.Patient;
import model.Specialist;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class PatientSchedule extends JFrame
{
        
    ArrayList<Specialist> specialists = OnlineBookingSystem.specialistType;
    ArrayList<Bookings> bookings = OnlineBookingSystem.bookingInfo;
    JPanel pane = new JPanel();
    JLabel jlbHospitalId = new JLabel("Hospital Id.");
    JTextField jtfHospitalId = new JTextField ("");
    
    JLabel jlbSpecialistType = new JLabel("Select Specialist.");
    JComboBox jcbSpecialistType = new JComboBox ();
    
    JLabel jlbDesiredDate = new JLabel("Desired date.");
    JTextField jtfDesiredDate = new JTextField ("");
          
    JButton jbtEnter = new JButton ("Click here to view all available slot to book appointment.");
    JButton jbtBookAppointment = new JButton ("Book Appointment");
    
    JPanel jplAvailableSlot = new JPanel();
    
    GridLayout gridl = new GridLayout(3, 2, 2, 2);
    
    JLabel jblAvailableSlotTitle = new JLabel("Specialist Available Slot");
    ButtonGroup bg = new ButtonGroup();
    
    JLabel jblSlotTime = new JLabel("Slot Time");
    JLabel jblSlotCode = new JLabel("Slot Code");
    
    JRadioButton jradSlotTime1 = new JRadioButton("08:00am");
    JLabel jblSlotCode1 = new JLabel("CM800am");
    
    JRadioButton jradSlotTime2 = new JRadioButton("10:00am");
    JLabel jblSlotCode2 = new JLabel("CM010am");
    
    
         
    PatientSchedule()        // the frame constructor
    {
        pane.setBounds(500,100,700,300);
        for(int i=0; i<specialists.size(); i++){
            jcbSpecialistType.addItem(specialists.get(i).getSpecialist_Name());
        }
      
      pane.setLayout(new GridLayout(12,1));
      pane.add(jlbHospitalId);
      pane.add(jtfHospitalId);
      //jtfHospitalId.setPreferredSize(new Dimension(20, 20));
      
      pane.add(jlbSpecialistType);
      pane.add(jcbSpecialistType);
      
      pane.add(jlbDesiredDate);
      pane.add(jtfDesiredDate);
      
      jblAvailableSlotTitle.setVisible(false);     
      
      jplAvailableSlot.setVisible(false);
      jbtBookAppointment.setVisible(false);
      
      jbtEnter.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) 
            {             
                Specialist mSpecialist = specialists.get(jcbSpecialistType.getSelectedIndex());
                String hospitalId = jtfHospitalId.getText();
                String desiredDate = jtfDesiredDate.getText();
           
                if (hospitalId.equals(""))
                {
                    JOptionPane.showMessageDialog(null,"Please enter hospital id.");
                    return;
                }
            
                if (desiredDate.equals(""))
                {
                    JOptionPane.showMessageDialog(null,"Please enter desired date.");
                    return;
                }
                AvailableSlots viewAvailableSlot = new AvailableSlots();
                viewAvailableSlot.setBounds(500,100,500,350);
                viewAvailableSlot.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });
      
//      jbtEnter.addActionListener(new ActionListener()
//      {
//        @Override
//        public void actionPerformed(ActionEvent e)
//        {          
//            
//           
//           
////            for(Bookings booking : bookings){            
////                 Patient pat = booking.getPatient();
////                 if(pat.equals(P))
////            }              
//            
//          jplAvailableSlot.setBounds(500,100,700,300);          
//          jplAvailableSlot.setLayout(gridl);
//          
//          jplAvailableSlot.add(jblSlotTime);
//          jplAvailableSlot.add(jblSlotCode);
//          
//          jplAvailableSlot.add(jradSlotTime1);          
//          jplAvailableSlot.add(jblSlotCode1);
//          
//          jplAvailableSlot.add(jradSlotTime2);          
//          jplAvailableSlot.add(jblSlotCode2);  
//          
//          bg.add(jradSlotTime1);
//          bg.add(jradSlotTime2);
//          
//          jplAvailableSlot.setVisible(true);
//          jblAvailableSlotTitle.setVisible(true);
//          jbtBookAppointment.setVisible(true);
//          jplAvailableSlot.setForeground(Color.BLUE);
//          
//        }
//      });
//           
      
      pane.add(jbtEnter);
      add(pane);
      
      setVisible(true); // make frame visible
      
    }
    
}
