/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import javax.swing.JFrame;

/**
 *
 * @author User
 */
public class PatientArrival extends JFrame
{
    private javax.swing.JButton jbtBookAppointment;
    private javax.swing.JLabel jlbHospitalId;
    private javax.swing.JLabel jblBookingCode;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jtfHospitalId;
    private javax.swing.JTextField jtfBookingCode;
    
    PatientArrival()
    {
        jPanel1 = new javax.swing.JPanel();
        jlbHospitalId = new javax.swing.JLabel();
        jtfHospitalId = new javax.swing.JTextField();
        jblBookingCode = new javax.swing.JLabel();
        jtfBookingCode = new javax.swing.JTextField();
        jbtBookAppointment = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Patient Arrival");
        getContentPane().setLayout(new java.awt.GridLayout(2, 2));

        jPanel1.setLayout(new java.awt.GridLayout(2, 2));

        jlbHospitalId.setText("Hospital Id:");
        jPanel1.add(jlbHospitalId);
        jPanel1.add(jtfHospitalId);

        jblBookingCode.setText("Booking Code:");
        jPanel1.add(jblBookingCode);
        jPanel1.add(jtfBookingCode);

        getContentPane().add(jPanel1);

        jbtBookAppointment.setText("See Specialist");
        getContentPane().add(jbtBookAppointment);

        pack();
        setVisible(true);
    }
}
