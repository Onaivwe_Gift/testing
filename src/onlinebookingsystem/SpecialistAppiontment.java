/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author User
 */
public class SpecialistAppiontment extends JFrame
{
    private javax.swing.JButton jbtViewPatient1;
    private javax.swing.JButton jbtViewPatient2;
    private javax.swing.JButton jbtViewPatient3;
    private javax.swing.JLabel jlbPatientCode;
    private javax.swing.JLabel jlbPatientStatus;
    private javax.swing.JLabel jblViewPatient;
    private javax.swing.JLabel jlbPatientCode1;
    private javax.swing.JLabel jlbPatientStatus1;
    private javax.swing.JLabel jlbPatientCode2;
    private javax.swing.JLabel jlbPatientStatus2;
    private javax.swing.JLabel jlbPatientCode3;
    private javax.swing.JLabel jlbPatientStatus3;
    
    SpecialistAppiontment()
    {
        setTitle("Specialist to view all appointment");
        jlbPatientCode = new javax.swing.JLabel();
        jlbPatientStatus = new javax.swing.JLabel();
        jblViewPatient = new javax.swing.JLabel();
        jlbPatientCode1 = new javax.swing.JLabel();
        jlbPatientStatus1 = new javax.swing.JLabel();
        jbtViewPatient1 = new javax.swing.JButton();
        jlbPatientCode2 = new javax.swing.JLabel();
        jlbPatientStatus2 = new javax.swing.JLabel();
        jbtViewPatient2 = new javax.swing.JButton();
        jlbPatientCode3 = new javax.swing.JLabel();
        jlbPatientStatus3 = new javax.swing.JLabel();
        jbtViewPatient3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(4, 3));

        jlbPatientCode.setText("Booking Codes");
        getContentPane().add(jlbPatientCode);

        jlbPatientStatus.setText("Status");
        getContentPane().add(jlbPatientStatus);

        jblViewPatient.setText("View Appointment");
        getContentPane().add(jblViewPatient);

        jlbPatientCode1.setText("CM008AM");
        getContentPane().add(jlbPatientCode1);

        jlbPatientStatus1.setText("WAITING");
        getContentPane().add(jlbPatientStatus1);

        jbtViewPatient1.setText("View Patient");
        getContentPane().add(jbtViewPatient1);

        jlbPatientCode2.setText("CM0010AM");
        getContentPane().add(jlbPatientCode2);

        jlbPatientStatus2.setText("IN_PROGRESS");
        getContentPane().add(jlbPatientStatus2);

        jbtViewPatient2.setText("View Patient");
        getContentPane().add(jbtViewPatient2);

        jlbPatientCode3.setText("CM0012PM");
        getContentPane().add(jlbPatientCode3);

        jlbPatientStatus3.setText("WAITING");
        getContentPane().add(jlbPatientStatus3);

        jbtViewPatient3.setText("View Patient");
        getContentPane().add(jbtViewPatient3);
        
        jbtViewPatient1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                PatientInfo patientInfoFrame = new PatientInfo();
                patientInfoFrame.setBounds(500,100,400,200);
                patientInfoFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                
            }
        });
        
        jbtViewPatient2.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                PatientInfo patientInfoFrame = new PatientInfo();
                patientInfoFrame.setBounds(500,100,400,200);
                patientInfoFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                
            }
        });
        
        jbtViewPatient3.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                PatientInfo patientInfoFrame = new PatientInfo();
                patientInfoFrame.setBounds(500,100,400,200);
                patientInfoFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                
            }
        });

        pack();
        setVisible(true);
    }
    
}
