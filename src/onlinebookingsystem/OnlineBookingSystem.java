/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import model.Bookings;
import model.Patient;
import model.Specialist;

/**
 *
 * @author User
 */
public class OnlineBookingSystem extends JFrame 
{
    
    
    JPanel pane = new JPanel();
    JLabel paneTitle = new JLabel("Welcome to Newgate Online Booking System.");
    JLabel paneNavigationInfo = new JLabel("Please use below buttons to navigate by clicking on it.");
    JButton jbtPatientSchedule = new JButton ("Patient Schedule");
    JButton jbtPatientArrival = new JButton ("Patient Arrival");
    JButton jbtSpecialist = new JButton ("Specialist");
    JButton jbtAdministrator = new JButton ("Administrator");
   // JComboBox<String> SpecialistTy = new JComboBox<>(Specialist_Type);
    
   // jbtPatientSchedule.
    public static final ArrayList<Specialist> specialistType = new ArrayList<>();
    public static final ArrayList<Patient> patientInfo = new ArrayList<>();    
    public static final ArrayList<Bookings> bookingInfo = new ArrayList<>();
         
    OnlineBookingSystem()        // the frame constructor
    {
        
      //super("Online System"); 
      setTitle("Newgate Online Booking System.");
      setBounds(500,100,700,300);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      Container con = this.getContentPane(); // inherit main frame
      con.add(pane);    // JPanel containers default to FlowLayout
      pane.add(paneTitle);
      pane.add(paneNavigationInfo);
      pane.add(jbtPatientSchedule);
      pane.add(jbtPatientArrival);
      pane.add(jbtSpecialist);      
      pane.add(jbtAdministrator);
     // pane.add(SpecialistTy);
//      getCloseButton().addActionListener(new Listener());
      
      jbtPatientSchedule.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
               PatientSchedule patientScheduleFrame = new PatientSchedule();
               patientScheduleFrame.setBounds(500,100,550,600);
               patientScheduleFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
               
            }
        });
      
      jbtPatientArrival.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
               PatientArrival patientArrivalFrame = new PatientArrival();
               patientArrivalFrame.setBounds(500,100,250,200);
               patientArrivalFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
               
            }
        });
      
      jbtSpecialist.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              
                onlinebookingsystem.Specialist specialistFrame = new onlinebookingsystem.Specialist();
                specialistFrame.setBounds(500,100,420,150);
                specialistFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                
            }
        });
      
      
      setVisible(true); // make frame visible
      
    }
  
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) 
    { 
        specialistType.add(new Specialist(01, "Cardiac Specialist"));
        specialistType.add(new Specialist(02, "Renal Specialist"));
        specialistType.add(new Specialist(03, "Allergist Specialist"));
        specialistType.add(new Specialist(04, "ENT Specialist"));
        specialistType.add(new Specialist(05, "Paediatric Specialist"));
        
        patientInfo.add(new Patient(001, "Mary"));
        patientInfo.add(new Patient(002, "Stanley"));
        patientInfo.add(new Patient(001, "Gift"));
        
        ArrayList<Specialist> CDspec = new ArrayList<>();
        CDspec.add(specialistType.get(0));
        
        ArrayList<Specialist> AGspec = new ArrayList<>();
        AGspec.add(specialistType.get(2));
        
        ArrayList<Specialist> PTspec = new ArrayList<>();
        PTspec.add(specialistType.get(4));
        
        bookingInfo.add(new Bookings(CDspec, patientInfo.get(1), "09-04-2015", Bookings.WAITING, "CM008AM", 1));
        bookingInfo.add(new Bookings(CDspec, patientInfo.get(2), "09-06-2015", Bookings.IN_PROGRESS, "CM010AM", 2));
        
        bookingInfo.add(new Bookings(AGspec, patientInfo.get(1), "09-04-2015", Bookings.WAITING, "AG008AM", 1));
        bookingInfo.add(new Bookings(AGspec, patientInfo.get(2), "04-06-2015", Bookings.FINISH, "AG011PM", 2));
        
        bookingInfo.add(new Bookings(PTspec, patientInfo.get(2), "09-04-2015", Bookings.IN_PROGRESS, "PT008AM", 2));
        bookingInfo.add(new Bookings(PTspec, patientInfo.get(1), "09-06-2015", Bookings.IN_PROGRESS, "PT010AM", 1));
        

        JFrame frame = new OnlineBookingSystem();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
}
