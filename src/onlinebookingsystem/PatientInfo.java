/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinebookingsystem;

import javax.swing.JFrame;

/**
 *
 * @author User
 */
public class PatientInfo extends JFrame
{
    private javax.swing.JLabel jblPatId;
    private javax.swing.JLabel jblPatName;
    private javax.swing.JLabel jblViewPatId;
    private javax.swing.JLabel jblViewPatName;
    
    PatientInfo()
    {
        jblPatId = new javax.swing.JLabel();
        jblViewPatId = new javax.swing.JLabel();
        jblPatName = new javax.swing.JLabel();
        jblViewPatName = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Patient Information");
        getContentPane().setLayout(new java.awt.GridLayout(2, 2));

        jblPatId.setText("Patient ID");
        getContentPane().add(jblPatId);

        jblViewPatId.setText("display id");
        getContentPane().add(jblViewPatId);

        jblPatName.setText("Patient Name");
        getContentPane().add(jblPatName);

        jblViewPatName.setText("display Patient Name");
        getContentPane().add(jblViewPatName);

        pack();
        
        setVisible(true);
    }
    
}
